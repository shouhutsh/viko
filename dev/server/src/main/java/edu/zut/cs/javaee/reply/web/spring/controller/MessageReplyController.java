package edu.zut.cs.javaee.reply.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.message.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.zut.cs.javaee.reply.domain.MessageReply;
import edu.zut.cs.javaee.reply.service.MessageReplyManager;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/messageReply")
public class MessageReplyController extends
        GenericController<MessageReply, Long, MessageReplyManager> {

	MessageReplyManager messageReplyManager;

	@Autowired
	public void setMessageReplyManager(MessageReplyManager messageReplyManager) {
		this.messageReplyManager = messageReplyManager;
		this.manager = this.messageReplyManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/messageReply/index";
	}

    @RequestMapping(method = RequestMethod.POST, value = "/addReply.html")
    public void addReply(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // FIXME 填充字段还需要再考虑，包括页面
        MessageReply reply = new MessageReply();
        reply.setUid(user.getId());
        reply.setGid(1L);
        reply.setMid(Long.valueOf(request.getParameter("mid")));
        reply.setContent(request.getParameter("content"));
        messageReplyManager.save(reply);

        response.getWriter().write("OK!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/replies.json", produces = "application/json")
    @ResponseBody
    public List<MessageReply> getMessageReplies(HttpServletRequest request) {
        Long mid = Long.valueOf(request.getParameter("mid"));
        return messageReplyManager.getReplies(mid);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/replies.html")
    public ModelAndView replies(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("reply/replies");
        mav.addObject("mid", request.getParameter("mid"));
        return mav;
    }
}

