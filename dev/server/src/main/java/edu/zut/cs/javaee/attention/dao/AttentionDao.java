package edu.zut.cs.javaee.attention.dao;

import edu.zut.cs.javaee.attention.domain.Attention;
import edu.zut.cs.javaee.base.dao.GenericDao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface AttentionDao extends GenericDao<Attention, Long> {
    @Query("SELECT a FROM Attention a WHERE a.uid=:uid")
    public List<Attention> getAttentions(@Param("uid") Long uid);
}
