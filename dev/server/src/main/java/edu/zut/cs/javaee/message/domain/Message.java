package edu.zut.cs.javaee.message.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "T_MESSAGE")
public class Message extends BaseEntityDomain {

    @Column(name = "USER_ID", nullable = false)
    private Long uid;
    @Column(name = "COPY_ID", unique = false)
    private Long cid;
    @Column(name = "MESSAGE_URL")
    private String url;
    @Column(name = "MESSAGE_CONTENT", nullable = false)
    private String content;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
