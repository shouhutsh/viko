package edu.zut.cs.javaee.privateLetter.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.privateLetter.domain.PrivateLetter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrivateLetterDao extends GenericDao<PrivateLetter, Long> {
    @Query("SELECT p FROM PrivateLetter p WHERE p.ruid=:uid AND p.state=:state")
    public List<PrivateLetter> getPrivateLetters(@Param("uid") Long uid, @Param("state")PrivateLetter.READ_STATE state);
}
