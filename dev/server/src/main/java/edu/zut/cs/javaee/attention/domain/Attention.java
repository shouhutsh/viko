package edu.zut.cs.javaee.attention.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;
import javax.persistence.*;

@Entity
@Table(name = "T_ATTENTION")
public class Attention extends BaseEntityDomain {

    @Column(name = "USER_ID")
    private Long uid;
    @Column(name = "ATTENTION_USER_ID")
    private Long auid;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getAuid() {
        return auid;
    }

    public void setAuid(Long auid) {
        this.auid = auid;
    }

}
