package edu.zut.cs.javaee.base.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.user.domain.UserInfo;
import edu.zut.cs.javaee.user.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shouhutsh on 14-10-24.
 */
public class BaseUserDetailsService implements UserDetailsService {

    @Autowired
    private UserManager userManager;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserInfo userInfo = userManager.findByName(s);

        //我们默认所有用户都只有ROLE_USER一种权限
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        BaseUserDetails user = new BaseUserDetails(
                userInfo.getName(),
                userInfo.getPasswd(),
                authorities
        );
        user.setId(userInfo.getId());

        return user;
    }
}
