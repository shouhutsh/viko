package edu.zut.cs.javaee.feedback.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.feedback.domain.Feedback;

public interface FeedbackManager extends GenericManager<Feedback,Long> {

}
