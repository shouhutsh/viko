package edu.zut.cs.javaee.reply.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.reply.domain.MessageReply;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageReplyDao extends GenericDao<MessageReply, Long> {
    @Query("SELECT r FROM MessageReply r WHERE r.uid=:uid AND r.mid=:mid ORDER BY r.dateCreated")
    public List<MessageReply> getReplies(@Param("uid") Long uid, @Param("mid") Long mid);
}
