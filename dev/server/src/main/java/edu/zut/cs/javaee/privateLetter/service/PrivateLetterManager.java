package edu.zut.cs.javaee.privateLetter.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.privateLetter.domain.PrivateLetter;

import java.util.List;

public interface PrivateLetterManager extends GenericManager<PrivateLetter, Long> {
    public List<PrivateLetter> getUnreadPrivateLetter();
    public List<PrivateLetter> getReadedPrivateLetter();
}
