package edu.zut.cs.javaee.privateLetter.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.privateLetter.dao.PrivateLetterDao;
import edu.zut.cs.javaee.privateLetter.domain.PrivateLetter;
import edu.zut.cs.javaee.privateLetter.service.PrivateLetterManager;

import java.util.List;

@Service(value = "privateLetterManager")
public class PrivateLetterManagerImpl extends GenericManagerImpl<PrivateLetter, Long>
		implements PrivateLetterManager {
	
	PrivateLetterDao privateLetterDao;

	@Autowired
	public void setPrivateLetterDao(PrivateLetterDao privateLetterDao) {
		this.privateLetterDao = privateLetterDao;
		this.dao = this.privateLetterDao;
	}

    @Override
    public List<PrivateLetter> getUnreadPrivateLetter() {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return privateLetterDao.getPrivateLetters(user.getId(), PrivateLetter.READ_STATE.UNREAD);
    }

    @Override
    public List<PrivateLetter> getReadedPrivateLetter() {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return privateLetterDao.getPrivateLetters(user.getId(), PrivateLetter.READ_STATE.READED);
    }
}


