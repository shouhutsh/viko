package edu.zut.cs.javaee.attention.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.attention.dao.AttentionDao;
import edu.zut.cs.javaee.attention.domain.Attention;
import edu.zut.cs.javaee.attention.service.AttentionManager;
import edu.zut.cs.javaee.attention.dao.AttentionDao;

import java.util.List;

@Service("attentionManager")
public class AttentionManagerImpl extends GenericManagerImpl<Attention, Long>
		implements AttentionManager{
	
	AttentionDao attentionDao;
	
	@Autowired
	public void setAttentionDao(AttentionDao messageDao){
		this.attentionDao=messageDao;
		this.dao=this.attentionDao;
	}

    @Override
    public List<Attention> getAttentions() {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return attentionDao.getAttentions(user.getId());
    }
}
