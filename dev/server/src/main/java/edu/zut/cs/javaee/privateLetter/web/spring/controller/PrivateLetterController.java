package edu.zut.cs.javaee.privateLetter.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.zut.cs.javaee.privateLetter.domain.PrivateLetter;
import edu.zut.cs.javaee.privateLetter.service.PrivateLetterManager;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/privateLetter")
public class PrivateLetterController extends
        GenericController<PrivateLetter, Long, PrivateLetterManager> {
	PrivateLetterManager privateLetterManager;

	@Autowired
	public void setPrivateLetterManager(PrivateLetterManager privateLetterManager) {
		this.privateLetterManager = privateLetterManager;
		this.manager = this.privateLetterManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/PrivateLetter/index";
	}

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "/privateLetter/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add.html")
    public void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long ruid = Long.valueOf(request.getParameter("ruid"));
        String content = request.getParameter("content");

        PrivateLetter pl = new PrivateLetter();
        pl.setSuid(user.getId());
        pl.setRuid(ruid);
        pl.setPmcontent(content);
        pl.setState(PrivateLetter.READ_STATE.UNREAD);

        privateLetterManager.save(pl);
        response.getWriter().write("OK!");
    }


    @RequestMapping(method = RequestMethod.GET, value = "/privateLetter.json", produces = "application/json")
    @ResponseBody
    public List<PrivateLetter> getMessageReplies(HttpServletRequest request) {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return privateLetterManager.getUnreadPrivateLetter();
    }

}
