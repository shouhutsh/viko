package edu.zut.cs.javaee.favorite.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.zut.cs.javaee.favorite.domain.Favorite;
import edu.zut.cs.javaee.favorite.service.FavoriteManager;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/favorite")
public class FavoriteController extends GenericController<Favorite, Long, FavoriteManager> {
	FavoriteManager favoriteManager;
	@Autowired
	public void setFavoriteManager(FavoriteManager favoriteManager) {
		this.favoriteManager = favoriteManager;
		this.manager = this.favoriteManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/favorite/index";
	}

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "favorite/" + url;
    }

    // FIXME 这个为了简单起见而使用GET方法，但是应该是POST方法的
    @RequestMapping(method = RequestMethod.GET, value = "/add.html")
    public void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long mid = Long.valueOf(request.getParameter("mid"));
        Long fuid = Long.valueOf(request.getParameter("fuid"));

        Favorite f = new Favorite();
        f.setMid(mid);
        f.setFuid(fuid);
        f.setUid(user.getId());
        favoriteManager.save(f);

        response.getWriter().write("OK!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/favorite.json", produces = "application/json")
    @ResponseBody
    public List<Favorite> getAttentions() {
        return favoriteManager.getFavorite();
    }
}
