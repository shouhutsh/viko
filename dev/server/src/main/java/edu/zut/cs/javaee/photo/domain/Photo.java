package edu.zut.cs.javaee.photo.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/5/18 0018.
 */
@Entity
@Table(name = "T_PHOTO")
public class Photo extends BaseEntityDomain {

    @Column(name = "USER_ID", unique = false)
    private Long uid;
    @Column(name = "PHOTO_NAME", nullable = false)
    private String name;
    @Column(name = "PHOTO_URL", nullable = false)
    private String url;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
