package edu.zut.cs.javaee.transpond.web.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.transpond.domain.Transpond;
import edu.zut.cs.javaee.transpond.service.TranspondManager;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/transpond")
public class TranspondController extends GenericController<Transpond, Long, TranspondManager> {
	
	TranspondManager transpondManager;

	@Autowired
	public void setTranspondManager(TranspondManager transpondManager) {
		this.transpondManager = transpondManager;
		this.manager = this.transpondManager;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/transpond/index";
	}

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url) {
        return "transpond/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add.html")
    public void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long mid = Long.valueOf(request.getParameter("mid"));
        String content = request.getParameter("content");

        Transpond ts = new Transpond();
        ts.setUid(user.getId());
        ts.setMid(mid);
        ts.setContent(content);

        transpondManager.save(ts);
        response.getWriter().write("OK!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/transpond.json", produces = "application/json")
    @ResponseBody
    public List<Transpond> getAttentions() {
        return transpondManager.getTranspond();
    }
}
