package edu.zut.cs.javaee.message.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.message.domain.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageDao extends GenericDao<Message, Long> {
    @Query("SELECT m FROM Message m WHERE m.uid=:uid")
    public List<Message> findByUser(@Param("uid") Long uid, Pageable pageable);

    @Query("SELECT m FROM Message m, Attention a WHERE a.uid=:uid AND m.uid=a.auid")
    public List<Message> getAttentionMessages(@Param("uid") Long uid, Pageable pageable);
}
