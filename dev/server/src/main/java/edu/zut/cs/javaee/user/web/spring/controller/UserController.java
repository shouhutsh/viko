package edu.zut.cs.javaee.user.web.spring.controller;

import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.user.domain.UserInfo;
import edu.zut.cs.javaee.user.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
@Controller
@RequestMapping("/user")
public class UserController extends
        GenericController<UserInfo, Long, UserManager> {

    UserManager userManager;

    @Autowired
    public void setUserManager(UserManager userManager){
        this.userManager = userManager;
        this.manager = this.userManager;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index.html")
    public String index(HttpServletRequest request, HttpServletResponse response){
        return "/user/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "user/" + url;
    }

    @RequestMapping(value = "/logout/success.html")
    public String logoutSuccess() {
        String message = "Logout Success!";
        return "redirect:/user/login.html?message="+message;
    }


    @RequestMapping(value = "/register.html", method = RequestMethod.POST)
    public void register(@RequestParam(value = "username") String username,
                         @RequestParam(value = "password") String password,
                         HttpServletResponse response) throws IOException {
        UserInfo user = new UserInfo();
        user.setName(username);
        user.setPasswd(
                new Md5PasswordEncoder().encodePassword(password, username)
        );

        userManager.save(user);

        response.getWriter().write("Register OK!");
    }
}
