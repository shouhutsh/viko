package edu.zut.cs.javaee.photo.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.photo.dao.PhotoDao;
import edu.zut.cs.javaee.photo.domain.Photo;
import edu.zut.cs.javaee.photo.service.PhotoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ZL on 14-5-25.
 */
@Service("photoManager")
public class PhotoManagerImpl extends GenericManagerImpl<Photo, Long> implements PhotoManager {

    PhotoDao photoDao;

    @Autowired
    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
        this.dao = this.photoDao;
    }

    @Override
    public List<Photo> getPhotos() {
        BaseUserDetails user = (BaseUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return photoDao.getPhotos(user.getId());
    }
}
