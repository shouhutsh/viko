package edu.zut.cs.javaee.message.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.message.dao.MessageDao;
import edu.zut.cs.javaee.message.domain.Message;
import edu.zut.cs.javaee.message.service.MessageManager;
import edu.zut.cs.javaee.user.service.UserManager;

import java.util.List;

@Service("messageManager")
public class MessageManagerImpl extends GenericManagerImpl<Message, Long>
		implements MessageManager {

	MessageDao messageDao;

	@Autowired
	UserManager userManager;

	@Autowired
	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
		this.dao = this.messageDao;
	}

    @Override
    public List<Message> findByUser(Pageable pageable) {
        BaseUserDetails user = (BaseUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return messageDao.findByUser(user.getId(), pageable);
    }

    @Override
    public List<Message> getAttentionMessages(Pageable pageable) {
        BaseUserDetails user = (BaseUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return messageDao.getAttentionMessages(user.getId(), pageable);
    }
}
