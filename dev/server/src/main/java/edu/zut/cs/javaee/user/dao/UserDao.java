package edu.zut.cs.javaee.user.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.user.domain.UserInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
public interface UserDao extends GenericDao<UserInfo, Long> {
    @Query("SELECT u FROM UserInfo u WHERE u.name like :un")
    public UserInfo findByName(@Param("un") String username);
}
