package edu.zut.cs.javaee.favorite.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.favorite.domain.Favorite;

import java.util.List;

public interface FavoriteManager extends GenericManager<Favorite, Long> {
    public List<Favorite> getFavorite();
}
