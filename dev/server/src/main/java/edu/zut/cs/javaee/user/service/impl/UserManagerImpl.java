package edu.zut.cs.javaee.user.service.impl;

import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.user.dao.UserDao;
import edu.zut.cs.javaee.user.domain.UserInfo;
import edu.zut.cs.javaee.user.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
@Service("userManager")
//@Transactional
public class UserManagerImpl extends GenericManagerImpl<UserInfo, Long>
            implements UserManager{

    UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao){
        this.userDao = userDao;
        this.dao = this.userDao;
    }

    @Override
    public UserInfo findByName(String username) {
        return userDao.findByName(username);
    }

}
