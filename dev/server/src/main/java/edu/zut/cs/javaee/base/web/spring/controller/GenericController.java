package edu.zut.cs.javaee.base.web.spring.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;
import edu.zut.cs.javaee.base.service.GenericManager;

public abstract class GenericController<T extends BaseEntityDomain, PK extends Serializable, M extends GenericManager<T, PK>>
		extends MultiActionController {
	protected M manager;
	protected PK id;
	protected T model;
	protected Page<T> page;

	protected int pageNumber = 0;
	protected int pageSize = 20;

	protected Pageable pageable = new PageRequest(pageNumber, pageSize,
			new Sort(Direction.ASC, "id"));

	@RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public T create(@RequestBody T model) {
		this.model = this.manager.save(model);
		return this.model;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public void delete(@PathVariable Long id) throws IOException {
		this.manager.delete((PK) id);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Page<T> get(HttpServletRequest request, HttpServletResponse response) {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		if (StringUtils.isNotBlank(page)) {
			this.pageNumber = Integer.valueOf(page) - 1;
		} else {
			this.pageNumber = 0;
		}
		if (StringUtils.isNotBlank(limit)) {
			this.pageSize = Integer.valueOf(limit);
		}
		this.pageable = new PageRequest(this.pageNumber, this.pageSize,
				new Sort(Direction.ASC, "id"));
		this.page = this.manager.findAll(this.pageable);
		logger.info(this.page);
		return this.page;
	}

	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/{Id}", method = RequestMethod.GET, produces = "application/json")
	public T get(@PathVariable Long id) {
		this.model = this.manager.findById((PK) id);
		return this.model;
	}

	public abstract String index(HttpServletRequest request,
			HttpServletResponse response);

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public T update(@PathVariable Long id, @RequestBody T model) {
		model.setId(id);
		this.model = this.manager.save(model);
		return this.model;
	}
}
