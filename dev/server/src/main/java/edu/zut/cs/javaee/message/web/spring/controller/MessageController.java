package edu.zut.cs.javaee.message.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.user.domain.UserInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.zut.cs.javaee.message.domain.Message;
import edu.zut.cs.javaee.message.service.MessageManager;

import javax.persistence.criteria.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/messages")
public class MessageController extends
        GenericController<Message, Long, MessageManager> {

	MessageManager messageManager;

	@Autowired
	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
		this.manager = this.messageManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/message/index";
	}

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "message/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/message.html")
    public void add(@RequestParam("message") String message,
                    HttpServletResponse response) throws IOException {
        BaseUserDetails login = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Message msg = new Message();
        msg.setUid(login.getId());
        msg.setContent(message);

        messageManager.save(msg);
      //response.getWriter().write("OK!");
    }

    @RequestMapping(value = "/ownMessages.json", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Message> getOwnMessages(HttpServletRequest request) {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        if (StringUtils.isNotBlank(page)) {
            this.pageNumber = Integer.valueOf(page) - 1;
        } else {
            this.pageNumber = 0;
        }
        if (StringUtils.isNotBlank(limit)) {
            this.pageSize = Integer.valueOf(limit);
        }
        this.pageable = new PageRequest(this.pageNumber, this.pageSize,
                new Sort(Sort.Direction.DESC, "dateCreated"));
        return this.manager.findByUser(this.pageable);
    }

    @RequestMapping(value = "/attentionMessages.json", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Message> getAttentionMessages(HttpServletRequest request) {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        if (StringUtils.isNotBlank(page)) {
            this.pageNumber = Integer.valueOf(page) - 1;
        } else {
            this.pageNumber = 0;
        }
        if (StringUtils.isNotBlank(limit)) {
            this.pageSize = Integer.valueOf(limit);
        }
        this.pageable = new PageRequest(this.pageNumber, this.pageSize,
                new Sort(Sort.Direction.DESC, "dateCreated"));
        return this.manager.getAttentionMessages(this.pageable);
    }
}
