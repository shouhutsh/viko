package edu.zut.cs.javaee.transpond.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.transpond.domain.Transpond;

import java.util.List;

public interface TranspondManager extends GenericManager<Transpond, Long> {
    public List<Transpond> getTranspond();
}
