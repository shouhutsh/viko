package edu.zut.cs.javaee.feedback.service.impl;

import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.feedback.dao.FeedbackDao;
import edu.zut.cs.javaee.feedback.domain.Feedback;
import edu.zut.cs.javaee.feedback.service.FeedbackManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "feedbackManager")
public class FeedbackManagerImpl extends GenericManagerImpl<Feedback, Long>
		implements FeedbackManager{

	FeedbackDao feedbackdao;

	@Autowired
	public void setFeedbackDao(FeedbackDao feedbackDao) {
		this.feedbackdao = feedbackDao;
		this.dao = this.feedbackdao;
	}

}
