package edu.zut.cs.javaee.photo.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.photo.domain.Photo;

import java.util.List;

/**
 * Created by ZL on 14-5-25.
 */
public interface PhotoManager extends GenericManager<Photo,Long> {
    public List<Photo> getPhotos();
}
