package edu.zut.cs.javaee.reply.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.message.service.MessageManager;
import edu.zut.cs.javaee.user.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.reply.dao.MessageReplyDao;
import edu.zut.cs.javaee.reply.domain.MessageReply;
import edu.zut.cs.javaee.reply.service.MessageReplyManager;

import java.util.List;

@Service(value = "messageReplyManager")
public class MessageReplyManagerImpl extends GenericManagerImpl<MessageReply, Long>
		implements MessageReplyManager{

	MessageReplyDao messageReplyDao;

	@Autowired
	public void setMessageReplyDao(MessageReplyDao messageReplyDao) {
		this.messageReplyDao = messageReplyDao;
		this.dao = this.messageReplyDao;
	}

    @Override
    public List<MessageReply> getReplies(Long mid) {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return messageReplyDao.getReplies(user.getId(), mid);
    }
}
