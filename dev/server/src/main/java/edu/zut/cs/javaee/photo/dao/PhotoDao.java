package edu.zut.cs.javaee.photo.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.photo.domain.Photo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ZL on 14-5-25.
 */
public interface PhotoDao extends GenericDao<Photo,Long> {
    @Query("SELECT p FROM Photo p WHERE p.uid=:uid")
    public List<Photo> getPhotos(@Param("uid") Long uid);
}
