package edu.zut.cs.javaee.favorite.service.impl;
 
import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.favorite.domain.Favorite;
import edu.zut.cs.javaee.favorite.service.FavoriteManager;
import edu.zut.cs.javaee.favorite.dao.FavoriteDao;

import java.util.List;

@Service(value = "favoriteManager")
public class FavoriteManagerImpl extends GenericManagerImpl< Favorite,Long>
		implements FavoriteManager{
	
	FavoriteDao  favoriteDao;
	@Autowired
	public void setFavoriteDao(FavoriteDao favoriteDao) {
		this.favoriteDao = favoriteDao;
		this.dao=this.favoriteDao;
	}

    @Override
    public List<Favorite> getFavorite() {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return favoriteDao.getFavorite(user.getId());
    }
}
