package edu.zut.cs.javaee.feedback.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.feedback.domain.Feedback;
import edu.zut.cs.javaee.feedback.service.FeedbackManager;

import edu.zut.cs.javaee.message.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

@Controller
@RequestMapping("/feedback")
public class FeedbackController extends
        GenericController<Feedback, Long, FeedbackManager> {

	FeedbackManager feedbackManager;

	@Autowired
	public void setFeedbackManager(FeedbackManager feedbackManager) {
		this.feedbackManager = feedbackManager;
		this.manager = this.feedbackManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/feedback/index";
	}

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "/feedback/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add.html")
    public void add(@RequestParam("content")String content,
                      HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Feedback feedback = new Feedback();
        feedback.setUid(user.getId());
        feedback.setContent(content);

        feedbackManager.save(feedback);
        response.getWriter().write("OK!");
    }
}

