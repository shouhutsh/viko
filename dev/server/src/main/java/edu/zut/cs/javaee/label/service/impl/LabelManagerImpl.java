package edu.zut.cs.javaee.label.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.user.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.label.dao.LabelDao;
import edu.zut.cs.javaee.label.domain.Label;
import edu.zut.cs.javaee.label.service.LabelManager;

import java.util.List;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
@Service("labelManager")
//@Transactional
public class LabelManagerImpl extends GenericManagerImpl<Label, Long>
            implements LabelManager{

    LabelDao labelDao;

    @Autowired
    public void setLabelDao(LabelDao labelDao){
        this.labelDao = labelDao;
        this.dao = this.labelDao;
    }

    @Override
    public List<Label> getLabels() {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return labelDao.getLabels(user.getId());
    }

    @Override
    public List<UserInfo> getUsers(String label) {
        return labelDao.getUsers(label);
    }
}
