package edu.zut.cs.javaee.attention.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.message.domain.Message;
import edu.zut.cs.javaee.user.domain.UserInfo;
import edu.zut.cs.javaee.user.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.zut.cs.javaee.attention.domain.Attention;
import edu.zut.cs.javaee.attention.service.AttentionManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/attention")
public class AttentionController extends
        GenericController<Attention, Long, AttentionManager> {

	AttentionManager attentionManager;

    @Autowired
    UserManager userManager;

	@Autowired
	public void setMessageManager(AttentionManager attentionManager) {
		this.attentionManager = attentionManager;
		this.manager = this.attentionManager;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response){
		return "/attention/index";
	}

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "attention/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add.html")
    public void add(@RequestParam("name") String name, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserInfo auser = userManager.findByName(name);
        if(null == auser) {
            response.getWriter().write("Not find!");
            return;
        }

        List<Attention> attentions = attentionManager.getAttentions();
        for(Attention a : attentions){
            if(a.getAuid() == auser.getId()){
                response.getWriter().write("Have been concerned!");
                return;
            }
        }

        Attention attention = new Attention();
        attention.setUid(user.getId());
        attention.setAuid(auser.getId());
        attentionManager.save(attention);

        response.getWriter().write("OK!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/attention.json", produces = "application/json")
    @ResponseBody
    public List<Attention> getAttentions() {
        return attentionManager.getAttentions();
    }

    // FIXME 为了简便起见将此方法设为GET
    @RequestMapping(method = RequestMethod.GET, value = "/add.html")
    public void addGet(@RequestParam("auid") Long auid, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserInfo auser = userManager.findById(auid);
        if(null == auser) {
            response.getWriter().write("Not find!");
            return;
        }

        List<Attention> attentions = attentionManager.getAttentions();
        for(Attention a : attentions){
            if(a.getAuid() == auser.getId()){
                response.getWriter().write("Have been concerned!");
                return;
            }
        }

        Attention attention = new Attention();
        attention.setUid(user.getId());
        attention.setAuid(auser.getId());
        attentionManager.save(attention);

        response.getWriter().write("OK!");
    }
}
