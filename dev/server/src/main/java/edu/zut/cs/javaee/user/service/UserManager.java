package edu.zut.cs.javaee.user.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.user.domain.UserInfo;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
public interface UserManager extends GenericManager<UserInfo, Long> {
    public UserInfo findByName(String username);
}

