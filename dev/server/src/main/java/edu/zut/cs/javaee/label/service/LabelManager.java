package edu.zut.cs.javaee.label.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.label.domain.Label;
import edu.zut.cs.javaee.user.domain.UserInfo;

import java.util.List;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
public interface LabelManager extends GenericManager<Label, Long> {
    public List<Label> getLabels();
    public List<UserInfo> getUsers(String label);
}

