package edu.zut.cs.javaee.transpond.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "T_TRANSPOND")
public class Transpond extends BaseEntityDomain {

    @Column(name = "USER_ID", unique = false)
    private Long uid;
    @Column(name = "MESSAGE_ID", unique = false)
    private Long mid;
    @Column(name = "CONTENT")
    private String content;
    
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
