package edu.zut.cs.javaee.transpond.service.impl;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import edu.zut.cs.javaee.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.transpond.dao.TranspondDao;
import edu.zut.cs.javaee.transpond.domain.Transpond;
import edu.zut.cs.javaee.transpond.service.TranspondManager;

import java.util.List;

@Service(value = "transpondManager")
public class TranspondManagerImpl extends GenericManagerImpl<Transpond, Long>
		implements TranspondManager{

	TranspondDao transpondDao;

	@Autowired
	public void setTranspondDao(TranspondDao transpondDao) {
		this.transpondDao = transpondDao;
		this.dao = this.transpondDao;
	}

    @Override
    public List<Transpond> getTranspond() {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return transpondDao.getTranspond(user.getId());
    }
}
