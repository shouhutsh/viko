package edu.zut.cs.javaee.photo.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.photo.domain.Photo;
import edu.zut.cs.javaee.photo.service.PhotoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by ZL on 14-5-26.
 */
@Controller
@RequestMapping("/photo")
public class PhotoController extends GenericController<Photo,Long,PhotoManager> {
    PhotoManager photoManager ;

    @Autowired
    public void setPhotoManager(PhotoManager photoManager) {
        this.photoManager = photoManager;
        this.manager = this.photoManager;
    }

    @RequestMapping(method = RequestMethod.GET , value = "/index.html")
    public String index(HttpServletRequest request, HttpServletResponse response)
    {
        return "/photo/index";
    }

    @RequestMapping(method = RequestMethod.GET , value = "/photos.html")
    public String show(){
        return "/photo/photos";
    }

    @RequestMapping(method = RequestMethod.GET , value = "/photos.json", produces = "application/json")
    @ResponseBody
    public List<Photo> getPhotos(HttpServletRequest request, HttpServletResponse response){
        return photoManager.getPhotos();
    }

    @RequestMapping(value = "/upload.html", method = RequestMethod.POST)
    public String upload(@RequestParam(value = "photo", required = false) MultipartFile file,
                         HttpServletRequest request, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String path = request.getSession().getServletContext().getRealPath("/") + "/" + user.getName();
        String fileName = file.getOriginalFilename();
        File targetFile = new File(path, fileName);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        //保存
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            response.getWriter().write("上传失败！");
        }
        Photo photo = new Photo();
        photo.setUid(user.getId());
        photo.setName(fileName);
        photo.setUrl(targetFile.getAbsolutePath());
        photoManager.save(photo);

        return "/photo/upload_success";
        /*response.getWriter().write("上传成功！");

        // FIXME 下面一句只是为了方便测试
        response.getWriter().write("<a href='/photo/" + photo.getId() + ".html'>测试上传地址</a>");*/
    }

    @RequestMapping(value = "/{photoID}.html", method = RequestMethod.GET)
    public @ResponseBody
    byte[] upload1(@PathVariable Long photoID, HttpServletResponse response) throws IOException {
        File photo = new File(photoManager.findById(photoID).getUrl());
        if(photo.exists()){
            try{
                return FileCopyUtils.copyToByteArray(photo);
            } catch (IOException e) {
                response.getWriter().write("没有该文件！");
            }
        }
        return null;
    }
}
