package edu.zut.cs.javaee.favorite.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/5/18 0018.
 */
@Entity
@Table(name = "T_FAVORITE")
public class Favorite extends BaseEntityDomain {

    @Column(name = "MSG_USER_ID", unique = false)
    private Long uid;
    @Column(name = "FAV_USER_ID", unique = false)
    private Long fuid;
    @Column(name = "MESSAGE_ID", unique = false)
    private Long mid;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getFuid() {
        return fuid;
    }

    public void setFuid(Long fuid) {
        this.fuid = fuid;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }
}
