package edu.zut.cs.javaee.favorite.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.favorite.domain.Favorite;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FavoriteDao extends GenericDao<Favorite, Long> {
    @Query("SELECT f FROM Favorite f WHERE f.uid=:uid ORDER BY f.dateCreated")
    public List<Favorite> getFavorite(@Param("uid") Long uid);
}