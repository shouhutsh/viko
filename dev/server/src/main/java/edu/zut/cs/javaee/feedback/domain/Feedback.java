package edu.zut.cs.javaee.feedback.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.*;

@Entity
@Table(name = "T_FEEDBACK")
public class Feedback extends BaseEntityDomain {

    @Column(name = "USER_ID", unique = false)
    private Long uid;
    
    @Column(name = "CONTENT", unique = false)
    private String content;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
    
    
}