package edu.zut.cs.javaee.message.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.message.domain.Message;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MessageManager extends GenericManager<Message, Long> {
    public List<Message> findByUser(Pageable pageable);
    public List<Message> getAttentionMessages(Pageable pageable);
}
