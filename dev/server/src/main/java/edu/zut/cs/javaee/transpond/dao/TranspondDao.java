package edu.zut.cs.javaee.transpond.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.transpond.domain.Transpond;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TranspondDao extends GenericDao<Transpond, Long> {
    @Query("SELECT t FROM Transpond t WHERE t.uid=:uid ORDER BY t.dateCreated")
    public List<Transpond> getTranspond(@Param("uid") Long uid);
}
