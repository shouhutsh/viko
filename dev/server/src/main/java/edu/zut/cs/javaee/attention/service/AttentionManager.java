package edu.zut.cs.javaee.attention.service;

import edu.zut.cs.javaee.attention.domain.Attention;
import edu.zut.cs.javaee.base.service.GenericManager;

import java.util.List;

public interface AttentionManager extends GenericManager<Attention, Long> {
    public List<Attention> getAttentions();
}
