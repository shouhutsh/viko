package edu.zut.cs.javaee.label.domain;


import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/5/18 0018.
 */
@Entity
@Table(name = "T_LABEL")
public class Label extends BaseEntityDomain {

    @Column(name = "USER_ID", unique = false)
    private Long uid;
    @Column(name = "LABEL_NAME", nullable = false)
    private String name;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
