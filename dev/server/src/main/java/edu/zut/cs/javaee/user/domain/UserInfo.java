package edu.zut.cs.javaee.user.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Administrator on 2014/5/12.
 */
@Entity
@Table(name="T_USER_INFO")
public class UserInfo extends BaseEntityDomain {

    // 修改用户名为唯一字段，则用户登录使用这个字段
    @Column(name = "USER_NAME", unique = true, nullable = false)
    private String name;
    @Column(name = "USER_PASSWD", nullable = false)
    private String passwd;


    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
