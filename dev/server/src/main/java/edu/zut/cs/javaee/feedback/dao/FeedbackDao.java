package edu.zut.cs.javaee.feedback.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.feedback.domain.Feedback;

public interface FeedbackDao extends GenericDao<Feedback,Long> {

}
