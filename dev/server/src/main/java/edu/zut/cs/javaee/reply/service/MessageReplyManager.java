package edu.zut.cs.javaee.reply.service;

import edu.zut.cs.javaee.base.service.GenericManager;
import edu.zut.cs.javaee.reply.domain.MessageReply;

import java.util.List;

public interface MessageReplyManager extends GenericManager<MessageReply, Long> {
	public List<MessageReply> getReplies(Long mid);
}
