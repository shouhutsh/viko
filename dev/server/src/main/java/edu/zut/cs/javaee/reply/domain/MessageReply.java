package edu.zut.cs.javaee.reply.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;
import javax.persistence.*;
import java.lang.Long;
import java.lang.String;

/**
 * Created by Qi_2 on 2014/5/18 0018.
 */
@Entity
@Table(name = "T_MESSAGE_REPLY")
public class MessageReply extends BaseEntityDomain {

    @Column(name = "USER_ID", nullable = false)
    private Long uid;
    @Column(name = "MESSAGE_ID", nullable = false)
    private Long mid;
    @Column(name = "GROUP_ID", nullable = false)
    private Long gid;
    @Column(name = "REPLY_CONTENT", nullable = false)
    private String content;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public Long getGid() {
        return gid;
    }

    public void setGid(Long gid) {
        this.gid = gid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
