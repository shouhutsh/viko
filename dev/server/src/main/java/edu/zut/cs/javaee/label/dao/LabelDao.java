package edu.zut.cs.javaee.label.dao;

import edu.zut.cs.javaee.base.dao.GenericDao;
import edu.zut.cs.javaee.label.domain.Label;
import edu.zut.cs.javaee.user.domain.UserInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
public interface LabelDao extends GenericDao<Label, Long> {
    @Query("SELECT l FROM Label l WHERE l.uid=:uid")
    public List<Label> getLabels(@Param("uid") Long uid);

    @Query("SELECT u FROM UserInfo u, Label l WHERE l.name=:name AND l.uid=u.id")
    public List<UserInfo> getUsers(@Param("name") String label);
}
