package edu.zut.cs.javaee.label.web.spring.controller;

import edu.zut.cs.javaee.base.domain.BaseUserDetails;
import edu.zut.cs.javaee.base.web.spring.controller.GenericController;
import edu.zut.cs.javaee.user.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.zut.cs.javaee.label.domain.Label;
import edu.zut.cs.javaee.label.service.LabelManager;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
@Controller
@RequestMapping("/label")
public class LabelController extends
        GenericController<Label, Long, LabelManager> {

    LabelManager labelManager;

    @Autowired
    public void setUserManager(LabelManager labelManager){
        this.labelManager = labelManager;
        this.manager = this.labelManager;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index.html")
    public String index(HttpServletRequest request, HttpServletResponse response){
        return "/label/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String urlGet(@PathVariable String url){
        return "label/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add.html")
    public void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = request.getParameter("name");

        Label l = new Label();
        l.setUid(user.getId());
        l.setName(name);

        labelManager.save(l);
        response.getWriter().write("OK!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/label.json",  produces = "application/json")
    @ResponseBody
    public List<Label> getLabels(HttpServletRequest request) {
        BaseUserDetails user = (BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return labelManager.getLabels();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users.json",  produces = "application/json")
    @ResponseBody
    public List<UserInfo> getUsers(HttpServletRequest request) {
        String label = request.getParameter("name");
        return labelManager.getUsers(label);
    }

}

