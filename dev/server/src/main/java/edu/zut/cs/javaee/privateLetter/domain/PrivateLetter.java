package edu.zut.cs.javaee.privateLetter.domain;

import edu.zut.cs.javaee.base.domain.BaseEntityDomain;

import javax.persistence.*;

/**
 * Created by Qi_2 on 2014/5/18 0018.
 */
@Entity
@Table(name = "T_PRIVATE_LETTER")
public class PrivateLetter extends BaseEntityDomain {

    public enum READ_STATE {READED, UNREAD}

    @Column(name = "SEND_USER_ID", unique = false)
    private Long suid;
    @Column(name = "RECIVE_USER_ID", unique = false)
    private Long ruid;
    @Column(name = "PRIVATE_LATTER_CONTENT", nullable = false)
    private String pmcontent;
    @Column(name = "PRIVATE_LATTER_STATE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private READ_STATE state;

    public Long getSuid() {
        return suid;
    }

    public void setSuid(Long suid) {
        this.suid = suid;
    }

    public Long getRuid() {
        return ruid;
    }

    public void setRuid(Long ruid) {
        this.ruid = ruid;
    }

    public String getPmcontent() {
        return pmcontent;
    }

    public void setPmcontent(String pmcontent) {
        this.pmcontent = pmcontent;
    }

    public READ_STATE getState() {
        return state;
    }

    public void setState(READ_STATE state) {
        this.state = state;
    }
}
