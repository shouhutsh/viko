<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>User</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">PrivateLetter</h1>
<hr/>

<div id="info"></div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            type:"get",
            url:"/privateLetter/privateLetter.json",
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.suid + "</div>");
                    $("#info").append("<div>" + item.pmcontent + "</div>");
                    $("#info").append("<div>" + new Date(parseInt(item.dateCreated)).toLocaleString().replace(/:\d{1,2}$/,' ') + "</div>");

                    $("#info").append("<div><a href='/privateLetter/privateLetter.html?ruid=" + item.suid + "'>回复</a></div>");
                    $("#info").append("<hr />");

                    item.state = 0;
                    update(item);
                })
            }
        })
    });

    function update(data){
        // FIXME 这里有些错误，另外base包里边的东西不安全而且不能调用
        $.ajax({
            type: "put",
            url: basePath + "/privateLetter/" + data.id + ".json",
            contentType: "application/json",
            dataType: "json",
            data: data
        })
    }
</script>
</body>
</html>
