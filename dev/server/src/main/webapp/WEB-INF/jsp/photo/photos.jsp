<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>User</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">Photo</h1>
<hr/>
<a href="/resources/html/upload.html">上传照片</a>

<div id="info"></div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            type:"get",
            url:"/photo/photos.json",
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.name + "</div>");
                    $("#info").append("<img src='/photo/" + item.id + ".html' />");

                    $("#info").append("<hr />");
                })
            }
        })
    });
</script>

</body>
</html>
