<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>User</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">Label</h1>
<hr/>

<form id="form" action="/label/add.html" method="post">
    <textarea name="name"></textarea>
    <input type="submit" name="submit" value="确定"/>
</form>

<div id=info></div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            type:"get",
            url:"/label/label.json",
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.name + "</div>");

                    $("#info").append("<a href='/label/search.html?name="+ item.name +"'>查找</a>")
                    $("#info").append("<hr />");
                })
            }
        })
    });
</script>
</body>
</html>
