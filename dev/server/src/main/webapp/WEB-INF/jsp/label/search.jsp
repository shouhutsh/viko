<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>Search</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">Search</h1>
<hr/>

<div id=info></div>
<script type="text/javascript">
    function getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = location.search.substr(1).match(reg);
        if (r != null) return unescape(decodeURI(r[2])); return null;
    }

    $(function () {
        $.ajax({
            type:"get",
            url:"/label/users.json?name=" + getQueryString("name"),
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.id + "</div>");
                    $("#info").append("<div>" + item.name + "</div>");

                    $("#info").append("<div><a href='/attention/add.html?auid=" + item.id + "'>关注</a></div>");
                    $("#info").append("<hr />");
                })
            }
        })
    });
</script>
</body>
</html>