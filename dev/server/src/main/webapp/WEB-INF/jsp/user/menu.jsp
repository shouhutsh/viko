<%--suppress XmlPathReference --%>
<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>

<c:url value="/messages/home.html" var="homeUrl"/>
<c:url value="/messages/user.html" var="userUrl"/>
<c:url value="/user/logout.html" var="logoutUrl"/>
<c:url value="/photo/photos.html" var="photos" />
<c:url value="/attention/attention.html" var="attention"/>
<c:url value="/privateLetter/show.html" var="privateLetter"/>
<c:url value="/transpond/show.html" var="transpond"/>
<c:url value="/favorite/show.html" var="favorite" />
<c:url value="/label/show.html" var="label"/>

<c:url value="/feedback/feedback.html" var="feedback"/>

<div class="title">
    <%--<div class="logo">
        <h1>VIKO</h1>
    </div>--%>
    <div class="menu">
        <ul class="nav">
            <li class="active"><a href="${homeUrl}">主页</a> </li>
            <li><a href="${userUrl}">个人</a> </li>
            <li><a href="${photos}">照片</a> </li>
            <li><a href="${attention}">关注</a> </li>
            <li><a href="${privateLetter}">私信</a> </li>
            <li><a href="${transpond}">转发</a> </li>
            <li><a href="${favorite}">收藏</a> </li>
            <li><a href="${label}">标签</a> </li>

            <li><a href="${feedback}">反馈</a> </li>
            <li><a href="${logoutUrl}">退出</a> </li>
        </ul>
        <span id="menu-username"><%=SecurityContextHolder.getContext().getAuthentication().getName()%></span>
        <br style="clear:left"/>
    </div>
</div>

<%--
<div class="menu">
	<ul>
		<li><a href="${homeUrl}">主页</a></li>
		<li><a href="${userUrl}">个人</a></li>
        <li><a href="${photos}">照片</a></li>
        <li><a href="${attention}">关注</a></li>
        <li><a href="${privateLetter}">私信</a></li>
        <li><a href="${transpond}">转发</a></li>
        <li><a href="${favorite}">收藏</a></li>
        <li><a href="${label}">标签</a></li>

        <li><a href="${logoutUrl}">登出</a></li>
        <li><a href="${feedback}">反馈</a></li>
	</ul>
	<span id="menu-username"><%=SecurityContextHolder.getContext().getAuthentication().getName()%></span>
	<br style="clear:left"/>
</div>--%>