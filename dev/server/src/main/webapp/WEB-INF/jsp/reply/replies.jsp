<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>User</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">Reply</h1>
<hr/>

<form id="form" action="/messageReply/addReply.html" method="post">
    <textarea id="content" name="content"></textarea>

    <input id="mid" name="mid" type="hidden"/>
    <input type="submit" name="submit" onclick="send()" />
</form>

<div id="info"></div>
<script type="text/javascript">
    function getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = location.search.substr(1).match(reg);
        if (r != null) return unescape(decodeURI(r[2])); return null;
    }

    function send(){
        document.getElementById('mid').value = getQueryString('mid');
        $("#form").submit();
    }

    $(function () {
        $.ajax({
            type:"get",
            url:"/messageReply/replies.json?mid=" + getQueryString('mid'),
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.uid + "</div>");
                    $("#info").append("<div>" + item.content + "</div>");
                    $("#info").append("<div>" + new Date(parseInt(item.dateCreated)).toLocaleString().replace(/:\d{1,2}$/,' ') + "</div>");

                    $("#info").append("<hr />");
                })
            }
        })
    });
</script>
</body>
</html>
