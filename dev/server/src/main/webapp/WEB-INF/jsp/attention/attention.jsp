<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>User</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">Attention</h1>
<hr/>

<form id="form" action="/attention/add.html" method="post">
    <textarea name="name"></textarea>
    <input type="submit" name="submit" />
</form>

<div id=info></div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            type:"get",
            url:"/attention/attention.json",
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.auid + "</div>");

                    $("#info").append("<div><a href='/privateLetter/privateLetter.html?ruid=" + item.auid + "'>发私信</a></div>");
                    $("#info").append("<hr />");
                })
            }
        })
    });
</script>
</body>
</html>
