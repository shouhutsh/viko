<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
	<link rel="stylesheet" type="text/css" media="screen" href="/resources/css/style.css"/>

    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
	<title>User</title>
</head>

<body>
	<jsp:include page="../user/menu.jsp" />
	<h1>User</h1>
	<hr/>
    <div id="info"></div>
    <script type="text/javascript">
        $(function () {
            $.ajax({
                type:"get",
                url:"/messages/ownMessages.json?page=1&limit=100",
                dataType:"json",
                success: function(data) {
                    $.each(data, function(i, item) {
                        $("#info").append("<div>" + item.content + "</div>");
                        $("#info").append("<div>" + new Date(parseInt(item.dateCreated)).toLocaleString().replace(/:\d{1,2}$/,' ') + "</div>");

                        $("#info").append("<hr />");
                    })
                }
            })
        });
    </script>
</body>
</html>