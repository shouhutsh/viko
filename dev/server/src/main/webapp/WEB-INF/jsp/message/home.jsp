<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home.css"/>
    <script type="text/javascript" src="/resources/js/jquery-2.1.1.js"></script>
    <title>Home</title>
</head>

<body>
<jsp:include page="../user/menu.jsp" />
<h1 id="banner">Home</h1>
<hr/>

<form action="/messages/message.html" method="post">
    <textarea name="message" class="input-text" placeholder=""></textarea>

    <input type="submit" name="发表" value="发表"/>
</form>

<div id=info></div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            type:"get",
            url:"/messages/attentionMessages.json?page=1&limit=100",
            dataType:"json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#info").append("<div>" + item.uid + "</div>");
                    $("#info").append("<div>" + item.content + "</div>");
                    $("#info").append("<div>" + new Date(parseInt(item.dateCreated)).toLocaleString().replace(/:\d{1,2}$/,' ') + "</div>");

                    // FIXME 下边的链接需要改为自己的地址
                    $("#info").append("<a href='/transpond/transpond.html?mid="+ item.id +"'>转发</a>")
                    $("#info").append("<a href='/messageReply/replies.html?mid=" + item.id +"'>评论</a>")
                    $("#info").append("<a href='/favorite/add.html?mid=" + item.id + "&fuid=" + item.uid + "'>收藏</a>")
                    $("#info").append("<hr />");
                })
            }
        })
    });
</script>
</body>
</html>