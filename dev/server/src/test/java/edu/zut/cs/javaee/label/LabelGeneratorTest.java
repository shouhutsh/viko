package edu.zut.cs.javaee.label;


import edu.zut.cs.javaee.label.domain.Label;
import edu.zut.cs.javaee.label.service.LabelManager;
import edu.zut.cs.javaee.user.domain.UserInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class LabelGeneratorTest {
	@Autowired
    LabelManager labelManager;
	
	@Before
	public void setUp(){
		
	}
	
	@Test
	public void testGenerate(){
//        UserInfo u = new UserInfo();
//        u.setId(1L);
//        Message m = new Message();
//        m.setId(1L);

		int num = 100; 
		for(int i = 1; i <= num; ++i){
			Label mr = new Label();
			mr.setName("1");
			mr.setId(1L);
			this.labelManager.save(mr);
		}
	}
}
