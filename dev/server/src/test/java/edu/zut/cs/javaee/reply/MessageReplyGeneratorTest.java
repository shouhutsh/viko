package edu.zut.cs.javaee.reply;

import edu.zut.cs.javaee.reply.domain.MessageReply;
import edu.zut.cs.javaee.reply.service.MessageReplyManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class MessageReplyGeneratorTest {
	@Autowired
    MessageReplyManager messageReplyManager;
	
	@Before
	public void setUp(){
		
	}
	
	@Test
	public void testGenerate(){
//        UserInfo u = new UserInfo();
//        u.setId(1L);
//        Message m = new Message();
//        m.setId(1L);

		int num = 100; 
		for(int i = 1; i <= num; ++i){
			MessageReply mr = new MessageReply();
			mr.setUid(1L);
			mr.setMid(1L);
			mr.setGid(1L);
			mr.setContent("SB_"+i);
			this.messageReplyManager.save(mr);
		}
	}
}
