package edu.zut.cs.javaee.user;

import edu.zut.cs.javaee.user.domain.UserInfo;
import edu.zut.cs.javaee.user.service.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class UserInfoGeneratorTest {

    @Autowired
    UserManager userManager;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testGenerate() throws IOException {
        int num = 10;
        for(int i = 1; i < num; ++i){
            UserInfo u = new UserInfo();
            // 注意name有唯一性约束，密码用MD5加密，且用用户名作为盐值
            u.setName("NM_" + i);
            u.setPasswd(
                    new Md5PasswordEncoder().encodePassword("PW_" + i, u.getName())
            );

            this.userManager.save(u);
        }
    }

    @Test
    public void testFindByName(){
        UserInfo userInfo;
        for(int i = 1; i < 10; ++i) {
            userInfo = userManager.findByName("NM_" + i);
            assert (userInfo.getName().equals("NM_" + i));
        }
    }
}
