package edu.zut.cs.javaee.favorite;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.zut.cs.javaee.favorite.domain.Favorite;

import edu.zut.cs.javaee.favorite.service.FavoriteManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class FavoriteGeneratorTest {
	@Autowired
    FavoriteManager favoriteManager;
	@Before
	public void setUp(){
		
	}
	
	@Test
	public void testGenerate(){
//        UserInfo su = new UserInfo();
//        su.setId(1L);
//        UserInfo ru = new UserInfo();
//        ru.setId(2L);
//        UserInfo fuid = new UserInfo();
//        fuid.setId(3L);
//        Message mid = new Message();
//        mid.setId(1L);

		int num = 100; 
		for(int i = 1; i <= num; ++i){
			Favorite f=new Favorite();
			f.setUid(1L);
			f.setFuid(1L);
			f.setMid(1L);
		   
			this.favoriteManager.save(f);
		}
	}
	

}
