package edu.zut.cs.javaee.privateletter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.zut.cs.javaee.privateLetter.domain.PrivateLetter;
import edu.zut.cs.javaee.privateLetter.domain.PrivateLetter.READ_STATE;
import edu.zut.cs.javaee.privateLetter.service.PrivateLetterManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class PrivateLetterGeneratorTest {
	@Autowired
    PrivateLetterManager privateLetterManager;
	
	@Before
	public void setUp(){
		
	}
	
	@Test
	public void testGenerate(){

		int num = 100; 
		for(int i = 1; i <= num; ++i){
			PrivateLetter p = new PrivateLetter();
			p.setSuid(1L);
			p.setRuid(2L);
			p.setState(READ_STATE.READED);
			p.setPmcontent("privateletter_"+i);
			this.privateLetterManager.save(p);
		}
	}

}
