package edu.zut.cs.javaee.message;

import edu.zut.cs.javaee.user.domain.UserInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.zut.cs.javaee.message.domain.Message;
import edu.zut.cs.javaee.message.service.MessageManager;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class MessageGeneratorTest {

	@Autowired
	MessageManager messageManager;

	@Before
	public void setUp() {

	}

	@Test
	public void testGenerate() {
        int num = 100;
		for (int i = 1; i <= num; ++i) {
            Message m = new Message();
            m.setUid(1L);
            m.setContent("NM1_"+i);

            this.messageManager.save(m);
		}
	}
}
