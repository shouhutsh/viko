package edu.zut.cs.javaee.photo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.zut.cs.javaee.photo.domain.Photo;
import edu.zut.cs.javaee.photo.service.PhotoManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class PhotoManagerTest {

	@Autowired
	PhotoManager photoManager;
	
	@Test
	public void saveTest()
	{
		List<Photo> list = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			Photo photo =  new Photo();
			photo.setName("photo"+i);
			photo.setUrl("D:\\myPhoto");
			list.add(photo);
		}
		photoManager.save(list);
	}
}
