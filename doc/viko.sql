-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.14 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  8.1.0.4669
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 viko 的数据库结构
CREATE DATABASE IF NOT EXISTS `viko` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `viko`;


-- 导出  表 viko.attention 结构
CREATE TABLE IF NOT EXISTS `attention` (
  `Aid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL COMMENT '关注用户字',
  `attUid` int(11) NOT NULL COMMENT '被关注用户',
  PRIMARY KEY (`Aid`),
  KEY `FK_attention_user_info` (`Uid`),
  KEY `FK_attention_user_info_2` (`attUid`),
  CONSTRAINT `FK_attention_user_info` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`),
  CONSTRAINT `FK_attention_user_info_2` FOREIGN KEY (`attUid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户关注';

-- 数据导出被取消选择。


-- 导出  表 viko.copy 结构
CREATE TABLE IF NOT EXISTS `copy` (
  `Cid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL COMMENT '转发用户',
  `Mid` int(11) NOT NULL COMMENT '被转发消息（包含消息原创者）',
  `cpUid` int(11) NOT NULL COMMENT '被转发用户（从此人处转发）',
  PRIMARY KEY (`Cid`),
  KEY `FK_copy_user_info` (`Uid`),
  KEY `FK_copy_user_info_2` (`cpUid`),
  KEY `FK_copy_message` (`Mid`),
  CONSTRAINT `FK_copy_message` FOREIGN KEY (`Mid`) REFERENCES `message` (`Mid`),
  CONSTRAINT `FK_copy_user_info` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`),
  CONSTRAINT `FK_copy_user_info_2` FOREIGN KEY (`cpUid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户转发\r\n\r\n由于能力所限，我并未设想好转发时能够\r\n显示多个转发用户\r\n\r\n此设计最多只能显示两个用户\r\n例：A转发B，B显示@A\r\n      C转发B，C显示@B@A\r\n      D转发C，D显示@C@A';

-- 数据导出被取消选择。


-- 导出  表 viko.fav 结构
CREATE TABLE IF NOT EXISTS `fav` (
  `Fid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL COMMENT '用户',
  `Mid` int(11) NOT NULL COMMENT '消息',
  PRIMARY KEY (`Fid`),
  KEY `FK_fav_user_info` (`Uid`),
  KEY `FK_fav_message` (`Mid`),
  CONSTRAINT `FK_fav_message` FOREIGN KEY (`Mid`) REFERENCES `message` (`Mid`),
  CONSTRAINT `FK_fav_user_info` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户收藏';

-- 数据导出被取消选择。


-- 导出  表 viko.feedback 结构
CREATE TABLE IF NOT EXISTS `feedback` (
  `Fid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) DEFAULT '0',
  `Fcontent` varchar(200) NOT NULL DEFAULT '0',
  `Ftime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Fid`),
  KEY `FK_feedback_user_info` (`Uid`),
  CONSTRAINT `FK_feedback_user_info` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户回馈表';

-- 数据导出被取消选择。


-- 导出  表 viko.label 结构
CREATE TABLE IF NOT EXISTS `label` (
  `Lid` int(11) NOT NULL AUTO_INCREMENT,
  `Lname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Lid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签\r\n\r\n此标签应该由系统管理人员设置\r\n也即此标签不应由用户随意设置\r\n\r\n作用为可以使用户找到与自己\r\n同类的朋友';

-- 数据导出被取消选择。


-- 导出  表 viko.message 结构
CREATE TABLE IF NOT EXISTS `message` (
  `Mid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL,
  `Tid` int(11) DEFAULT NULL COMMENT '话题，用##圈起来的字段',
  `Cid` int(11) DEFAULT NULL COMMENT '转发，若此字段不为空则本消息表示转发并评论',
  `Murl` varchar(200) DEFAULT NULL COMMENT '连接，用于用户在消息里添加图片，视频等',
  `Mcontent` varchar(200) NOT NULL,
  `MCfav` int(10) unsigned zerofill NOT NULL,
  `MCreply` int(10) unsigned zerofill NOT NULL,
  `MCcopy` int(10) unsigned zerofill NOT NULL,
  `Mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Mid`),
  KEY `FK_message_userinfo` (`Uid`),
  KEY `FK_message_messagetitle` (`Tid`),
  KEY `FK_message_copy` (`Cid`),
  CONSTRAINT `FK_message_copy` FOREIGN KEY (`Cid`) REFERENCES `copy` (`Cid`),
  CONSTRAINT `FK_message_messagetitle` FOREIGN KEY (`Tid`) REFERENCES `message_title` (`Tid`),
  CONSTRAINT `FK_message_userinfo` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发表消息';

-- 数据导出被取消选择。


-- 导出  表 viko.message_reply 结构
CREATE TABLE IF NOT EXISTS `message_reply` (
  `Rid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL,
  `Mid` int(11) NOT NULL COMMENT '用于标识唯一的一条消息',
  `Gid` int(11) NOT NULL COMMENT '用于标识本消息中的一组回复',
  `Rtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用于标识一组回复中的某条',
  `Rcontent` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Rid`),
  KEY `FK_message_reply_message` (`Mid`),
  KEY `FK_message_reply_user_info` (`Uid`),
  CONSTRAINT `FK_message_reply_message` FOREIGN KEY (`Mid`) REFERENCES `message` (`Mid`),
  CONSTRAINT `FK_message_reply_user_info` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息回复';

-- 数据导出被取消选择。


-- 导出  表 viko.message_title 结构
CREATE TABLE IF NOT EXISTS `message_title` (
  `Tid` int(11) NOT NULL AUTO_INCREMENT,
  `Tnum` int(10) unsigned zerofill NOT NULL COMMENT '用于记录扩展此话题的消息数量',
  `Tname` varchar(50) NOT NULL,
  `Tdesc` varchar(200) DEFAULT NULL,
  `Ttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息话题';

-- 数据导出被取消选择。


-- 导出  表 viko.photo 结构
CREATE TABLE IF NOT EXISTS `photo` (
  `Pid` int(11) NOT NULL AUTO_INCREMENT,
  `Pname` varchar(50) NOT NULL,
  `Purl` varchar(200) NOT NULL COMMENT '此字段应该标识为该图片的路径',
  `Ptime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='照片';

-- 数据导出被取消选择。


-- 导出  表 viko.photo_floder 结构
CREATE TABLE IF NOT EXISTS `photo_floder` (
  `PFid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL,
  `PFcover` int(11) DEFAULT NULL COMMENT '封面图片，只能选取文件夹内的图片',
  `PFname` varchar(20) NOT NULL,
  `PFnum` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`PFid`),
  KEY `FK_photoFloder_usercount` (`Uid`),
  KEY `FK_photofloder_photo` (`PFcover`),
  CONSTRAINT `FK_photofloder_photo` FOREIGN KEY (`PFcover`) REFERENCES `photo` (`Pid`),
  CONSTRAINT `FK_photoFloder_usercount` FOREIGN KEY (`Uid`) REFERENCES `user_count` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片文件夹';

-- 数据导出被取消选择。


-- 导出  表 viko.pri_message 结构
CREATE TABLE IF NOT EXISTS `pri_message` (
  `PMid` int(11) NOT NULL AUTO_INCREMENT,
  `SUid` int(11) NOT NULL COMMENT '发送者',
  `RUid` int(11) NOT NULL COMMENT '接收者',
  `PMcontent` varchar(200) NOT NULL,
  `PMstate` enum('RD','UR') NOT NULL DEFAULT 'UR' COMMENT '状态，RD-->查看过，UR-->未查看',
  `PMtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PMid`),
  KEY `FK_priMessage_userinfo` (`SUid`),
  KEY `FK_priMessage_userinfo_2` (`RUid`),
  CONSTRAINT `FK_priMessage_userinfo` FOREIGN KEY (`SUid`) REFERENCES `user_info` (`Uid`),
  CONSTRAINT `FK_priMessage_userinfo_2` FOREIGN KEY (`RUid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='私信';

-- 数据导出被取消选择。


-- 导出  表 viko.school 结构
CREATE TABLE IF NOT EXISTS `school` (
  `Sid` int(11) NOT NULL AUTO_INCREMENT,
  `Sname` varchar(20) DEFAULT NULL,
  `Sstate` char(6) DEFAULT '000000',
  PRIMARY KEY (`Sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学校名称\r\n\r\n注意由于设计添加state字段\r\n因此此表并不都存的是学校\r\n还有省和市的信息\r\n此表也应由系统管理人员填写\r\n\r\n具体如下描述\r\nstate为char(6)，默认为000000\r\n第一组00表示省\r\n第二组00表示市\r\n第三组00表示学校\r\n\r\n这样设计方便级联查询\r\n';

-- 数据导出被取消选择。


-- 导出  表 viko.user_count 结构
CREATE TABLE IF NOT EXISTS `user_count` (
  `Uid` int(11) NOT NULL,
  `UCfar` int(10) unsigned zerofill NOT NULL COMMENT '用户收藏数',
  `UCcopy` int(10) unsigned zerofill NOT NULL COMMENT '用户转发数',
  `UCatten` int(10) unsigned zerofill NOT NULL COMMENT '用户关注数',
  `UCfans` int(10) unsigned zerofill NOT NULL COMMENT '用户粉丝数',
  `UCmsg` int(10) unsigned zerofill NOT NULL COMMENT '用户发布消息数',
  `UCprimsg` int(10) unsigned zerofill NOT NULL COMMENT '用户未读私信数',
  PRIMARY KEY (`Uid`),
  CONSTRAINT `FK__userinfo` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户用于计数的表\r\n\r\n此表应该同user_info合为一个\r\n但由于此表完全是冗余表，只是\r\n为了统计方便而设，我目前暂不\r\n确定这个设计是否是对的，因此\r\n将两个表拆分开';

-- 数据导出被取消选择。


-- 导出  表 viko.user_info 结构
CREATE TABLE IF NOT EXISTS `user_info` (
  `Uid` int(11) NOT NULL AUTO_INCREMENT,
  `Uschool` int(11) NOT NULL,
  `Ualais` varchar(20) NOT NULL DEFAULT '0',
  `Ulogin` varchar(64) NOT NULL DEFAULT '0' COMMENT '以后可能存密文',
  `Upasswd` varchar(20) NOT NULL DEFAULT '0',
  `Usex` enum('O','G','M') NOT NULL DEFAULT 'O',
  `Uname` varchar(20) DEFAULT NULL,
  `Uqq` varchar(20) DEFAULT NULL,
  `Uwork` varchar(20) DEFAULT NULL,
  `Utel` varchar(20) DEFAULT NULL,
  `Uinfo` varchar(200) DEFAULT NULL,
  `Ublog` varchar(100) DEFAULT NULL,
  `Utime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Uid`),
  KEY `FK_userinfo_school` (`Uschool`),
  CONSTRAINT `FK_userinfo_school` FOREIGN KEY (`Uschool`) REFERENCES `school` (`Sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息\r\n\r\n其中包含了用户登录的信息和详细信息';

-- 数据导出被取消选择。


-- 导出  表 viko.user_label 结构
CREATE TABLE IF NOT EXISTS `user_label` (
  `ULid` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` int(11) NOT NULL,
  `Lid` int(11) NOT NULL,
  PRIMARY KEY (`ULid`),
  KEY `FK_user_label_user_info` (`Uid`),
  KEY `FK_user_label_label` (`Lid`),
  CONSTRAINT `FK_user_label_label` FOREIGN KEY (`Lid`) REFERENCES `label` (`Lid`),
  CONSTRAINT `FK_user_label_user_info` FOREIGN KEY (`Uid`) REFERENCES `user_info` (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户标签\r\n\r\n用户自我评价，方便找到与自己同类型的人';

-- 数据导出被取消选择。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
